#include<stdio.h>
#include<stdlib.h>
#include<string.h>
 
int main() {
   FILE *fp1, *fp2;
   int c;
   char *line = NULL;
   size_t len = 0;
   ssize_t read;
 
   fp1 = fopen("TreeNames.txt", "r");
   if (fp1 == NULL) {
      puts("cannot open this file");
      exit(1);
   }
   fp2 = fopen("sqlqueries.sql", "w");
   if (fp2 == NULL) {
      puts("Not able to open this file");
      exit(1);
   }

    while ((read = getline(&line, &len, fp1)) != -1) {
        //printf("Retrieved line of length %zu :\n", read);
        //printf("%s", line);
        int i = 0;
        char *p = strtok (line, " ");
        char *array[2];

        while (p != NULL)
        {
            array[i++] = p;
            p = strtok (NULL, " ");
            p=strtok(p, "\n");
        }

        fprintf(fp2,"select distinct genus, species from species where genus = \"%s\" AND species = \"%s\";\n",array[0],array[1]);
        fprintf(fp2,"select count(distinct site_name) from site where site_id in (select site_id from species where genus = \"%s\" AND species = \"%s\");\n",array[0],array[1]);

    }

    fclose(fp1);
    fclose(fp2);
    return 1;
 
   
 
  
}