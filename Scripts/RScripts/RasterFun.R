require(raster)
require(rgdal)


fis <- unlist(list.files("~/Desktop/RisForResearch/bioclim"))
length(fis)
dir <- "~/Desktop/RisForResearch/bioclim"
ras <- c()
for(i in 1:length(fis)){
  spl <- unlist(strsplit(fis[i],"[.]"))
  if(spl[2]=="bil"){
    ras <-c(ras,fis[i])
  }
}
ras

rasdir <-c()
for(i in 1:length(ras)){
  x <- paste(dir,ras[i],sep="/")
  rasdir <-c(rasdir,x)
}

ext<-extent(c(-129,-113.5,32.5,53)) ################ for W NA
for(i in 1:length(rasdir)){
  r <-raster(rasdir[i])
  rc <- crop(r,ext)
  ex <- unlist(strsplit(ras[i],"[.]"))
  ex <- paste(ex[1],"asc",sep=".")
  outp<-paste("~/Desktop/RisForResearch/bioclimCA",ex,sep="/")
  writeRaster(rc,outp,overwrite=T,datatype="ascii")
}
x<-raster(outp)
plot(x)




ext<-extent(c(-166,-54,14,69)) ################ for NA
for(i in 1:length(rasdir)){
  r <-raster(rasdir[i])
  rc <- crop(r,ext)
  ex <- unlist(strsplit(ras[i],"[.]"))
  ex <- paste(ex[1],"asc",sep=".")
  outp<-paste("~/Desktop/RisForResearch/bioclimNA",ex,sep="/")
  writeRaster(rc,outp,overwrite=T,datatype="ascii")
}
x<-raster(outp)
plot(x)



ext<-extent(c(-124.6,-116.27,32.5,42.1)) ################ for CA
for(i in 1:length(rasdir)){
  r <-raster(rasdir[i])
  rc <- crop(r,ext)
  ex <- unlist(strsplit(ras[i],"[.]"))
  ex <- paste(ex[1],"asc",sep=".")
  outp<-paste("~/Desktop/RisForResearch/bioclimCA",ex,sep="/")
  writeRaster(rc,outp,overwrite=T,datatype="ascii")
}

x=raster("~/Desktop/RisForResearch/bioclimNA/bio1.asc")
plot(x)
