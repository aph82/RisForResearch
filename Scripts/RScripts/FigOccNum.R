Species <- c("A. magnifica","P. albicaulis","P. balfouriana","P. contorta", "P. jeffreyi")
na <- c(271,304,289,89,354)
sn <- c(44,40,12,57,41)
require(gridExtra)
df <- t(data.frame(Species,na,sn,stringsAsFactors = F))
row.names(df) <- c("Species","North America Occurrences","Sierra Nevada Occurrences")
df
grid.table(df)
?grid.table
