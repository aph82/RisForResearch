setwd("~/Desktop/")
al<-read.csv("RisForResearch/Chosen\ Trees/Pinus_albicaulis.csv",header=F)
alt<- raster("~/Desktop/RisForResearch/CAalt")
al<-data.frame(al)
alalt<- extract(alt,al)
lat<-al$V2
lalt<-data.frame(lat,alalt)
plot(lalt)

lr<- lm(alalt ~ lat)
abline(lr)
summary(lr)
lr


files <- list.files(path="~/Desktop/RisForResearch/Chosen\ Trees", pattern="*.csv", full.names=T, recursive=FALSE)

lats<-c()
alts<-c()

for(i in 1:length(files)){
  t <- read.csv(files[i], header=F) # load file
  t<-data.frame(t)
  alalt<- extract(alt,t)
  lat<-t$V2
  lalt<-data.frame(lat,alalt)
  
  lats<-c(lats,lat)
  alts<-c(alts,alalt)
}

lalts<-data.frame(lats,alts)
plot(lalts)
lr<- lm(alts ~ lats)
abline(lr)
summary(lr)
lr

plot(lalts,main="Relationship Between Altitude and Latitude in Species Distributions",xlab="Latitude of Species Occurrence",ylab="Altitude of Species Occurrence")
