dir<-"~/Desktop/RisForResearch/RESULTS"
species <- list.files("~/Desktop/RisForResearch/RESULTS")
ws <- list.files("~/Desktop/RisForResearch/RESULTS/Abies concolor")
who <-list.files("~/Desktop/RisForResearch/RESULTS/Abies concolor/Whole")
sie <- list.files("~/Desktop/RisForResearch/RESULTS/Abies concolor/Sierra")

inputdir<- c()

for(i in 2:length(species)){
  sws<- paste(dir,species[i],ws,sep="/")
  
  #swsw <- paste(sws[3],who,sep="/")
  inputd <- paste(sws[2],sie,sep="/")
  #inputd <- c(swsw,swss)
  sp<-paste(species[i],".csv",sep="")
  inputd <- paste(inputd,sp,sep="/")
  inputdir <- c(inputdir,inputd)
}
inputdir
inputdir<-gsub(" ","\\\\\\\\ ",inputdir)



outdir<-c()
for(i in 2:length(species)){
  sws<- paste(dir,species[i],ws,sep="/")
  
  #swsw <- paste(sws[3],who,sep="/")
  outd <- paste(sws[2],sie,sep="/")
  outd <- paste(outd,"maxent",sep="/")
  outdir <- c(outdir,outd)
}
outdir
outdir<-gsub(" ","\\\\\\\\ ",outdir)

testdir<-c()
for(i in 1:length(inputdir)){
  qq <- unlist(strsplit(inputdir[i],"/"))
  qq[7] <- "TEST"
  qq<-paste(qq,collapse="/")
  testdir<-c(testdir,qq)
}
testdir
testdir<-gsub(" ","\\\\ ",testdir)

mcom<-c()

for(i in 1:length(outdir)){
  makedir <- "mkdir -p xxx"
  mcom<- c(mcom,gsub("xxx",outdir[i],makedir))
  runmax <- "java -mx512m -jar ~/Desktop/maxent/maxent.jar environmentallayers=~/Desktop/RisForResearch/bioclimCA/ samplesfile=xxxin outputdirectory=xxxout redoifexists autorun adjustsampleradius=-7 jackknife writebackgroundpredictions testsamplesfile=xxxtest"
  inte<-gsub("xxxin",inputdir[i],runmax)
  inte<-gsub("xxxout",outdir[i],inte)
  inte<-gsub("xxxtest",testdir[i],inte)
  mcom<-c(mcom,inte)
}
mcom<-c(mcom,"cd ~/Desktop/RisForResearch","git add .","git commit -m \"The real CA Job\"","git push")
mcom

write(mcom,file="~/Desktop/RisForResearch/MaxentBatchTEST/mcom")
system("chmod +x ~/Desktop/RisForResearch/MaxentBatchTEST/mcom")