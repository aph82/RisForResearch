#!/usr/bin/perl
use strict;
print "continue";
open FILE, '/Users/averyhill/Desktop/RisforResearch/TreeNames.txt';

my $line = <>;

my @lines = <FILE>;


open(my $fh, '>', 'Script.pulls.tree.lon.lat.from.sql');
for(@lines){
	my @binomial = split ( /\s/, $_ );
	my $genus = @binomial[0];

	my $species = @binomial[1];
	my $table_name = join ('_',@binomial);

	
	print $fh "drop table if exists $table_name;\n";
	print $fh "create table $table_name (lon text, lat text);\n";
	print $fh "insert into $table_name (lon,lat) select lon,lat from site where site_id in (select site_id from species where genus = \"$genus\" AND species = \"$species\");\n";
	print $fh ".mode csv\n";
	print $fh ".once $table_name.csv\n";
	print $fh "select * from $table_name;\n";
	#print $fh ".save $table_name.csv\n";
	print $fh "drop table if exists $table_name;\n";
	print $fh "\n";
}

close $fh;