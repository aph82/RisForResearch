drop table if exists Abies_concolor;
create table Abies_concolor (lon text, lat text);
insert into Abies_concolor (lon,lat) select lon,lat from site where site_id in (select site_id from species where genus = "Abies" AND species = "concolor");
.mode csv
.once Abies_concolor.csv
select * from Abies_concolor;
drop table if exists Abies_concolor;

drop table if exists Abies_magnifica;
create table Abies_magnifica (lon text, lat text);
insert into Abies_magnifica (lon,lat) select lon,lat from site where site_id in (select site_id from species where genus = "Abies" AND species = "magnifica");
.mode csv
.once Abies_magnifica.csv
select * from Abies_magnifica;
drop table if exists Abies_magnifica;

drop table if exists Artemisia_tridentata;
create table Artemisia_tridentata (lon text, lat text);
insert into Artemisia_tridentata (lon,lat) select lon,lat from site where site_id in (select site_id from species where genus = "Artemisia" AND species = "tridentata");
.mode csv
.once Artemisia_tridentata.csv
select * from Artemisia_tridentata;
drop table if exists Artemisia_tridentata;

drop table if exists Cercocarpus_ledifolius;
create table Cercocarpus_ledifolius (lon text, lat text);
insert into Cercocarpus_ledifolius (lon,lat) select lon,lat from site where site_id in (select site_id from species where genus = "Cercocarpus" AND species = "ledifolius");
.mode csv
.once Cercocarpus_ledifolius.csv
select * from Cercocarpus_ledifolius;
drop table if exists Cercocarpus_ledifolius;

drop table if exists Juniperus_occidentalis;
create table Juniperus_occidentalis (lon text, lat text);
insert into Juniperus_occidentalis (lon,lat) select lon,lat from site where site_id in (select site_id from species where genus = "Juniperus" AND species = "occidentalis");
.mode csv
.once Juniperus_occidentalis.csv
select * from Juniperus_occidentalis;
drop table if exists Juniperus_occidentalis;

drop table if exists Pinus_albicaulis;
create table Pinus_albicaulis (lon text, lat text);
insert into Pinus_albicaulis (lon,lat) select lon,lat from site where site_id in (select site_id from species where genus = "Pinus" AND species = "albicaulis");
.mode csv
.once Pinus_albicaulis.csv
select * from Pinus_albicaulis;
drop table if exists Pinus_albicaulis;

drop table if exists Pinus_balfouriana;
create table Pinus_balfouriana (lon text, lat text);
insert into Pinus_balfouriana (lon,lat) select lon,lat from site where site_id in (select site_id from species where genus = "Pinus" AND species = "balfouriana");
.mode csv
.once Pinus_balfouriana.csv
select * from Pinus_balfouriana;
drop table if exists Pinus_balfouriana;

drop table if exists Pinus_contorta;
create table Pinus_contorta (lon text, lat text);
insert into Pinus_contorta (lon,lat) select lon,lat from site where site_id in (select site_id from species where genus = "Pinus" AND species = "contorta");
.mode csv
.once Pinus_contorta.csv
select * from Pinus_contorta;
drop table if exists Pinus_contorta;

drop table if exists Pinus_flexilis;
create table Pinus_flexilis (lon text, lat text);
insert into Pinus_flexilis (lon,lat) select lon,lat from site where site_id in (select site_id from species where genus = "Pinus" AND species = "flexilis");
.mode csv
.once Pinus_flexilis.csv
select * from Pinus_flexilis;
drop table if exists Pinus_flexilis;

drop table if exists Pinus_jeffreyi;
create table Pinus_jeffreyi (lon text, lat text);
insert into Pinus_jeffreyi (lon,lat) select lon,lat from site where site_id in (select site_id from species where genus = "Pinus" AND species = "jeffreyi");
.mode csv
.once Pinus_jeffreyi.csv
select * from Pinus_jeffreyi;
drop table if exists Pinus_jeffreyi;

drop table if exists Pinus_lambertiana;
create table Pinus_lambertiana (lon text, lat text);
insert into Pinus_lambertiana (lon,lat) select lon,lat from site where site_id in (select site_id from species where genus = "Pinus" AND species = "lambertiana");
.mode csv
.once Pinus_lambertiana.csv
select * from Pinus_lambertiana;
drop table if exists Pinus_lambertiana;

drop table if exists Pinus_monophylla;
create table Pinus_monophylla (lon text, lat text);
insert into Pinus_monophylla (lon,lat) select lon,lat from site where site_id in (select site_id from species where genus = "Pinus" AND species = "monophylla");
.mode csv
.once Pinus_monophylla.csv
select * from Pinus_monophylla;
drop table if exists Pinus_monophylla;

drop table if exists Pinus_monticola;
create table Pinus_monticola (lon text, lat text);
insert into Pinus_monticola (lon,lat) select lon,lat from site where site_id in (select site_id from species where genus = "Pinus" AND species = "monticola");
.mode csv
.once Pinus_monticola.csv
select * from Pinus_monticola;
drop table if exists Pinus_monticola;

drop table if exists Pinus_ponderosa;
create table Pinus_ponderosa (lon text, lat text);
insert into Pinus_ponderosa (lon,lat) select lon,lat from site where site_id in (select site_id from species where genus = "Pinus" AND species = "ponderosa");
.mode csv
.once Pinus_ponderosa.csv
select * from Pinus_ponderosa;
drop table if exists Pinus_ponderosa;

drop table if exists Populus_tremuloides;
create table Populus_tremuloides (lon text, lat text);
insert into Populus_tremuloides (lon,lat) select lon,lat from site where site_id in (select site_id from species where genus = "Populus" AND species = "tremuloides");
.mode csv
.once Populus_tremuloides.csv
select * from Populus_tremuloides;
drop table if exists Populus_tremuloides;

drop table if exists Tsuga_mertensiana;
create table Tsuga_mertensiana (lon text, lat text);
insert into Tsuga_mertensiana (lon,lat) select lon,lat from site where site_id in (select site_id from species where genus = "Tsuga" AND species = "mertensiana");
.mode csv
.once Tsuga_mertensiana.csv
select * from Tsuga_mertensiana;
drop table if exists Tsuga_mertensiana;

