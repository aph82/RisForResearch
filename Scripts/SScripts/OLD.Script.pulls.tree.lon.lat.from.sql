.mode csv
.output TreeCoords.csv

create table Abies_concolor (id INTEGER PRIMARY KEY AUTOINCREMENT, lon text, lat text);
insert into Abies_concolor (lon,lat) select distinct lon,lat from species, site where genus = "Abies" AND species = "concolor";

create table Abies_magnifica (id INTEGER PRIMARY KEY AUTOINCREMENT, lon text, lat text);
insert into Abies_magnifica (lon,lat) select distinct lon,lat from species, site where genus = "Abies" AND species = "magnifica";

create table Artemisia_tridentata (id INTEGER PRIMARY KEY AUTOINCREMENT, lon text, lat text);
insert into Artemisia_tridentata (lon,lat) select distinct lon,lat from species, site where genus = "Artemisia" AND species = "tridentata";

create table Cercocarpus_ledifolius (id INTEGER PRIMARY KEY AUTOINCREMENT, lon text, lat text);
insert into Cercocarpus_ledifolius (lon,lat) select distinct lon,lat from species, site where genus = "Cercocarpus" AND species = "ledifolius";

create table Juniperus_occidentalis (id INTEGER PRIMARY KEY AUTOINCREMENT, lon text, lat text);
insert into Juniperus_occidentalis (lon,lat) select distinct lon,lat from species, site where genus = "Juniperus" AND species = "occidentalis";

create table Pinus_albicaulis (id INTEGER PRIMARY KEY AUTOINCREMENT, lon text, lat text);
insert into Pinus_albicaulis (lon,lat) select distinct lon,lat from species, site where genus = "Pinus" AND species = "albicaulis";

create table Pinus_balfouriana (id INTEGER PRIMARY KEY AUTOINCREMENT, lon text, lat text);
insert into Pinus_balfouriana (lon,lat) select distinct lon,lat from species, site where genus = "Pinus" AND species = "balfouriana";

create table Pinus_contorta (id INTEGER PRIMARY KEY AUTOINCREMENT, lon text, lat text);
insert into Pinus_contorta (lon,lat) select distinct lon,lat from species, site where genus = "Pinus" AND species = "contorta";

create table Pinus_flexilis (id INTEGER PRIMARY KEY AUTOINCREMENT, lon text, lat text);
insert into Pinus_flexilis (lon,lat) select distinct lon,lat from species, site where genus = "Pinus" AND species = "flexilis";

create table Pinus_jeffreyi (id INTEGER PRIMARY KEY AUTOINCREMENT, lon text, lat text);
insert into Pinus_jeffreyi (lon,lat) select distinct lon,lat from species, site where genus = "Pinus" AND species = "jeffreyi";

create table Pinus_lambertiana (id INTEGER PRIMARY KEY AUTOINCREMENT, lon text, lat text);
insert into Pinus_lambertiana (lon,lat) select distinct lon,lat from species, site where genus = "Pinus" AND species = "lambertiana";

create table Pinus_monophylla (id INTEGER PRIMARY KEY AUTOINCREMENT, lon text, lat text);
insert into Pinus_monophylla (lon,lat) select distinct lon,lat from species, site where genus = "Pinus" AND species = "monophylla";

create table Pinus_monticola (id INTEGER PRIMARY KEY AUTOINCREMENT, lon text, lat text);
insert into Pinus_monticola (lon,lat) select distinct lon,lat from species, site where genus = "Pinus" AND species = "monticola";

create table Pinus_ponderosa (id INTEGER PRIMARY KEY AUTOINCREMENT, lon text, lat text);
insert into Pinus_ponderosa (lon,lat) select distinct lon,lat from species, site where genus = "Pinus" AND species = "ponderosa";

create table Populus_tremuloides (id INTEGER PRIMARY KEY AUTOINCREMENT, lon text, lat text);
insert into Populus_tremuloides (lon,lat) select distinct lon,lat from species, site where genus = "Populus" AND species = "tremuloides";

create table Tsuga_mertensiana (id INTEGER PRIMARY KEY AUTOINCREMENT, lon text, lat text);
insert into Tsuga_mertensiana (lon,lat) select distinct lon,lat from species, site where genus = "Tsuga" AND species = "mertensiana";
